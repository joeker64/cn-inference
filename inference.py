"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
Inferece.py:
    gRPC server which will read frames on the service "getSteam" and output data on location & bounding boxes of objects detected by the ML module.
    Currently, yolov3 and yolov3 tiny are supported   
"""

import json
import numpy as np
import time
import cv2
import os
import argparse
import tvm
from concurrent import futures
import grpc

from src.argconfig import ArgConf
from src.grpc import Datas_pb2
from src.grpc import Datas_pb2_grpc
from src.load_model_config import load_model_config, load_model
from src.preprocess import normalize_and_reshape

#load arguments
ap = argparse.ArgumentParser()
args = ArgConf(ap)

# Load model configuration
print("----------Loading model configuration for : ", args["model"], "----------------------")
postprocess, score_threshold, mask, anchors, model_path, max_outputs, iou_threshold, names, strides, num_classes = load_model_config(args["model"], args["cpu_cores"])
#load model
print("-----------------------------Done-------------------------------------")
print("-------------------------Loading model: ", args["model"], "---------------------------")
# load the model.
start = time.time()

model = load_model(model_path)

end = time.time()                                                                                                           
model_load_time = (end-start)
print("------------------------------------Done-------------------------------------")

class Inference(Datas_pb2_grpc.MainServerServicer):
    """
    Creates class for the computation of the service "getStream". Once frame is recived on service, the frame is configured for the module then will be inferenced
    This process is all timed and the inference time is printed to stdout 
    """                                                                         
    def __init__(self):                                                                                                     
        pass                                                                                                                
    def getStream(self, request_iterator, context):                                                                         
        for req in request_iterator:                                                                                        
            start = time.time()                                                                                             
            dBuf = np.frombuffer(req.datas, dtype=np.uint8)                                     
            img = cv2.imdecode(dBuf, cv2.IMREAD_COLOR)                                          
            img = normalize_and_reshape(img)
            model.set_input("input_1", tvm.nd.array(img))
            start_run = time.time()
            model.run()
            end_run = time.time()
            print("Inference api time : ", end_run - start_run)
            right_boxes, right_classes, right_scores = postprocess(model, anchors, mask, strides, max_outputs, 
                                                                   iou_threshold, score_threshold, num_classes)
            inference_time = 1/(end_run - start_run)                                                                                                            
            if right_boxes is None:                                                                                                                   
                msg = json.dumps({"right_boxes":0, "right_classes":0, 
                                  "right_scores":0, "model_load":str(model_load_time), 
                                  "inf_time":str(inference_time)})
            else:                                                                                                                                     
                msg = json.dumps({"right_boxes":right_boxes.tolist(), 
                                  "right_classes":right_classes.tolist(), 
                                  "right_scores":right_scores.tolist(), 
                                  "model_load":str(model_load_time), 
                                  "inf_time":str(inference_time)})
            end = time.time()            
            print("----------------------Total Inference_time: ", end - start, "-------------------")                                                                                                                                
            yield Datas_pb2.Reply(reply=msg)                                                                                                          


def serve():
    """
    Starts gRPC server with 10 threads
    """                                                                                                                               
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))                                                                                  
    Datas_pb2_grpc.add_MainServerServicer_to_server(Inference(), server)                                                                              
    server.add_insecure_port('[::]:'+ args["port"])                                                                                                   
    server.start()                                                                                                                                    
    print('---------------------------GRPC server started-------------------------')
    print("---------------------------GRPC Port number :", args["port"], "-------------------------")                                                                                                                 
    server.wait_for_termination()                                                                                                                     
                                                                                                                                                      
if __name__ == '__main__':                                                                                                                            
    serve() 


