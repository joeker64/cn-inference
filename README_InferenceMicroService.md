
# Building the ML inference container

## Setup for ML inference container on Edge device

- Run the below command on the target device (ARM System Ready Device)
```
$ cd <PROJECT_ROOT>
```
## Building the docker container
- Change the working directory to ML inference contianer folder
```sh
$ cd <PROJECT_ROOT>
```
- Build the docker container
```sh
$ docker build -t <IMAGE_NAME>:<IMAGE_TAG> .
```
- Update IMAGE_NAME and IMAGE_TAG. These can be given according to user choice eg. inference_container:v1

- Check the docker images list with the command below
```sh
$ docker images
```
## Running the docker container. 
- Use the below command to run the docker container to launch ML inference container 
    ```sh
    $ docker run -it --network=host <IMAGAE_NAME>:<IMAGE_TAG> -p <INF_PORT> -m <MODEL> -c <CPU_CORES_OF_DEVICE>
    ```
    - Port number (-p) is 8080 preferable and should be given during running of application container.
    - Model name (-m) should be loaded for inference. Currently yolov3 and tiny_yolov3 models are supported. 
    - Number of CPU cores available (-c) in the device. This argument is optional and has the default number as 4.

## Deployment of inference container from cloud
- For deploying the container from AWS cloud refer to Deployment of inference container from AWS cloud from docs/ARM-Smart_camera_CSP_Alibaba_Inference_container_deployment.pdf
- For deploying the container from Alibaba cloud refer to Deployment of inference container from Alibaba cloud from docs/ARM-Smart_camera_CSP_Alibaba_Inference_container_deployment.pdf