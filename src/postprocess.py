"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
Postprocess.py:
    Computation needed after inference has been ran
	Supported models are: yolov3, yolov3_tiny 
"""
import numpy as np

def _nms_boxes(boxes, scores, iou_thresh=0.45):
	"""
	Uses the Non-maxiumum suppression (nms) algorthim with Intersection of Union (IoU) to calculate a single bounding box for the detected object

	Args:
		boxes (int array): List of boxes surrounding objects detected after inference
		scores (float array): List of scores of accuracy of detected objects after inference

	Returns:
		keep (int array): List of filtered boxes
	"""
	x = boxes[:, 0]
	y = boxes[:, 1]
	w = boxes[:, 2] -  boxes[:, 0]
	h = boxes[:, 3] -  boxes[:, 1]

	areas = (w)* (h)
	order = scores.argsort()[::-1]

	keep = []
	while order.size > 0:
		i = order[0]
		keep.append(i)

		xx1 = np.maximum(x[i], x[order[1:]])
		yy1 = np.maximum(y[i], y[order[1:]])
		xx2 = np.minimum(x[i] + w[i], x[order[1:]] + w[order[1:]])
		yy2 = np.minimum(y[i] + h[i], y[order[1:]] + h[order[1:]])

		w1 = np.maximum(0.0, xx2 - xx1 + 1)
		h1 = np.maximum(0.0, yy2 - yy1 + 1)
		inter = w1 * h1

		ovr = inter / (areas[i] + areas[order[1:]] - inter)
		inds = np.where(ovr <= iou_thresh)[0]
		order = order[inds + 1]

	keep = np.array(keep)
	return keep

def sigmoid(x):
	return (1 / (1 + np.exp(-x)))

def post_process_predictions(inputs, anchors, mask, strides, max_outputs, iou_threshold, score_threshold, ind, num_classes=2):
	"""
	Downsize the image with the stride layer and calculate all properties A(x,y,w,h,score, class[S]) for S amount of classes/detectable objects and
	A amount of anchors

	Args:
		inputs (numpy array): A single layer of the output of yolo/tiny yolo detection layer/inferece   
        anchors (int-matrix): Marix containing predefined bounding boxes known as anchor boxes
		mask (int-matrix): Matrix used for ML masking
		strides (int array): Configures number and size of stride layers for downsampling the image 
        max_outputs (int):Max objects to be detected in a single frame
        iou_threshold (float): Threshold for the Intersection over Union (IoU) algorthim 
        score_threshold (float): Minimum value class score for it to be valid
        ind (int): Index of the output of inference layer array. Coincides with inputs
		num_classes (int): Total number of objects/classes the model can detect. Default value of 2

	Returns:
		all_boxes (numpy array): All possible drawn boxes/anchors for each gridcell in the frame
		all_scores (numpy array): All scores for varible "all_boxes"
		all_classes (numpy array): All classes/objects for varible "all_boxes" and "all_scores"
	"""
	dtype = inputs[0].dtype
	logits = inputs[0]
	i = ind
	anchors, stride = anchors[mask[i]], strides[i]
	x_shape = np.shape(logits)
	logits = np.reshape(logits, (x_shape[0], x_shape[1], x_shape[2], len(anchors), num_classes + 5))

	box_xy, box_wh, obj, cls = logits[...,:2], logits[...,2:4], logits[...,4], logits[...,5:]
	box_xy = sigmoid(box_xy)
	obj = sigmoid(obj)
	cls = sigmoid(cls)

	grid_shape = x_shape[1:3]
	grid_h, grid_w = grid_shape[0], grid_shape[1]
	anchors = np.array(anchors, dtype=np.float32)
	grid = np.meshgrid(np.arange(grid_w), np.arange(grid_h))
	grid = np.expand_dims(np.stack(grid, axis=-1), axis=2)  # [gx, gy, 1, 2]

	box_xy = (box_xy + np.array(grid, dtype=np.float32)) * stride
	box_wh = np.exp(box_wh) * np.array(anchors, dtype=np.float32)

	box_x1y1 = box_xy - box_wh / 2.
	box_x2y2 = box_xy + box_wh / 2.
	box = np.concatenate([box_x1y1, box_x2y2], axis=-1)

	all_boxes = np.reshape(box, (x_shape[0], -1, 1, 4))
	objects = np.reshape(obj, (x_shape[0], -1, 1))
	all_classes = np.reshape(cls, (x_shape[0], -1, num_classes))

	all_scores = objects * all_classes
	return all_boxes, all_scores, all_classes

def filter_boxes(all_boxes, all_scores, all_classes, score_thresh=0.2, iou_thresh=0.45):
	"""
	Reduce boxes for gridcells using Non-maxiumum suppression (nms) algorthim with Intersection of Union (IoU) 

	Args:
		all_boxes (numpy array): All possible drawn boxes/anchors for each gridcell in the frame
		all_scores (numpy array): All scores for varible "all_boxes"
		all_classes (numpy array): All classes for varible "all_boxes" and "all_scores"
		iou_threshold (float): Threshold for the Intersection over Union (IoU) algorthim 
        score_threshold (float): Minimum value class score for it to be valid 

	Returns:
		nboxes (numpy array): Filtered boxes on layer
		nclasses (numpy array): Filtered All classes/objects for varible nboxes and nscores
		nscores (numpy array): Filtered scores for varible nboxes
	"""
	box_classes = np.argmax(all_scores, axis=-1)
	box_class_scores = np.max(all_scores, axis=-1)
	pos = np.where(box_class_scores >= score_thresh)

	fil_boxes = all_boxes[pos]
	fil_boxes = np.reshape(fil_boxes, (fil_boxes.shape[0], fil_boxes.shape[2]))
	fil_classes = box_classes[pos]
	fil_scores = box_class_scores[pos]

	nboxes, nclasses, nscores = [], [], []
	for c in set(fil_classes):
		inds = np.where(fil_classes == c)
		b = fil_boxes[inds]
		c = fil_classes[inds]
		s = fil_scores[inds]

		keep = _nms_boxes(b, s)

		nboxes.append(b[keep])
		nclasses.append(c[keep])
		nscores.append(s[keep])

	if not nclasses and not nscores:
		return None, None, None

	return np.concatenate(nboxes), np.concatenate(nclasses), np.concatenate(nscores)

def postprocess_yolo(model, anchors, mask, strides, max_outputs, iou_threshold, score_threshold, num_classes):
	"""
	Take input from the inference/output layers of the yolov3 ML model and calculate & filter bounding boxes, scores and classes for the layer

	Args:
		model (TVM.GraphModule): Runtime graph module that has been used to execute the graph.   
        anchors (int-matrix): Marix containing predefined bounding boxes known as anchor boxes
		mask (int-matrix): Matrix used for ML masking
		strides (int array): Configures number and size of stride layers for downsampling the image 
        max_outputs (int):Max objects to be detected in a single frame
        iou_threshold (float):Threshold for the Intersection over Union (IoU) algorthim 
        score_threshold (float): Minimum value class score for it to be valid 
		num_classes (int): Total number of classes the model can detect.

	Returns:
		right_boxes (numpy array): Boxes to be drawn on frame to highlight class
		right_classes (numpy array): Class names used to describe each box in "right_boxes"
		right_scores (numpy_array) Scores used to describe accuraccy of detection of class & boxes
	"""
    #out0, out1, out2 = model.get_tensor(output_details[0]['index']), model.get_tensor(output_details[1]['index']), model.get_tensor(output_details[2]['index'])
	out0, out1, out2 = model.get_output(0).asnumpy(), model.get_output(1).asnumpy(), model.get_output(2).asnumpy()
	big_all_boxes, big_all_scores, big_all_classes = post_process_predictions([out0], anchors, mask, strides, max_outputs, iou_threshold, score_threshold, 0, num_classes)
	mid_all_boxes, mid_all_scores, mid_all_classes = post_process_predictions([out1], anchors, mask, strides, max_outputs, iou_threshold, score_threshold, 1, num_classes)
	small_all_boxes, small_all_scores, small_all_classes = post_process_predictions([out2], anchors, mask, strides, max_outputs, iou_threshold, score_threshold, 2, num_classes)

	bboxes = np.concatenate([big_all_boxes, mid_all_boxes, small_all_boxes], axis=1)
	scores = np.concatenate([big_all_scores, mid_all_scores, small_all_scores], axis=1)
	classes = np.concatenate([big_all_classes, mid_all_classes, small_all_classes], axis=1)
	right_boxes, right_classes, right_scores = filter_boxes(bboxes, scores, classes, score_threshold, iou_threshold)
	return right_boxes, right_classes, right_scores

def postprocess_tiny(model, anchors, mask, strides, max_outputs, iou_threshold, score_threshold, num_classes):
	"""
	Take input from the inference/output layers of the yolov3 tiny ML model and calculate & filter bounding boxes, scores and classes for the layer
	 
	Args:
		model (TVM.GraphModule): Runtime graph module that has been used to execute the graph.   
        anchors (int-matrix): Marix containing predefined bounding boxes known as anchor boxes
		mask (int-matrix): Matrix used for ML masking
		strides (int array): Configures number and size of stride layers for downsampling the image 
        max_outputs (int): Max objects to be detected in a single frame
        iou_threshold (float): Threshold for the Intersection over Union (IoU) algorthim 
        score_threshold (float): Minimum value class score for it to be valid
		num_classes (int): Total number of classes the model can detect.

	Returns:
		right_boxes (numpy array): Boxes to be drawn on frame to highlight class
		right_classes (numpy array): Class names used to describe each box in "right_boxes"
		right_scores (numpy_array) Scores used to describe accuraccy of detection of class & boxes
	"""
	out0, out1 = model.get_output(0).asnumpy(), model.get_output(1).asnumpy()
    #out0, out1 = model.get_tensor(output_details[0]['index']), model.get_tensor(output_details[1]['index'])\
	big_all_boxes, big_all_scores, big_all_classes = post_process_predictions([out0], anchors, mask, strides, max_outputs, iou_threshold, score_threshold, 0, num_classes)
	small_all_boxes, small_all_scores, small_all_classes = post_process_predictions([out1], anchors, mask, strides, max_outputs, iou_threshold, score_threshold, 1, num_classes)
	
	bboxes = np.concatenate([big_all_boxes, small_all_boxes], axis=1)
	scores = np.concatenate([big_all_scores, small_all_scores], axis=1)
	classes = np.concatenate([big_all_classes, small_all_classes], axis=1)
	right_boxes, right_classes, right_scores = filter_boxes(bboxes, scores, classes, score_threshold, iou_threshold)
	return right_boxes, right_classes, right_scores

