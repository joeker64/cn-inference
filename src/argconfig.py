"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

def ArgConf(ap):
    """
    Provides command line arguments to inference.py

    Args:
        ap (argparse.ArgumentParser): Argument parser class

    Returns:
        args (argparse.ArgumentParser): Argument parser class with added arguments
    """
    ap.add_argument("-p","--port", required=True, help="Port number(not 5000)")
    ap.add_argument("-m","--model", required=True, help="Model to be used tiny_yolov3/yolov3")
    ap.add_argument("-c","--cpu_cores", default="4", required=False, help="CPU cores on the device")
    args = vars(ap.parse_args())
    return args