"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
Load_model_config.py:
    Loads configuation from files for inference. Provides custom configs for supported models which are: 
        tiny_yolov3
        yolov3
"""
import json
import os
import tvm
from tvm.contrib import graph_runtime
import numpy as np

from src.preprocess import decode_class_names
from src.postprocess import filter_boxes, _nms_boxes, sigmoid, postprocess_tiny, postprocess_yolo

def load_model(model_path):
    """
    Loads 3 files to create our ML model for inference: TVM module library, TVM model parameters blobs and TVM execution graph 
    These files are created in the compilation container 

    Args:
        model_path (string): Location of directory where files are located

    Returns:
        model (TVM.GraphModule): Runtime graph module that can be used to execute the graph.
    """
    ctx = tvm.cpu()
    loaded_json = open(model_path + "deploy_graph.json").read()
    loaded_lib = tvm.runtime.load_module(model_path + "deploy_lib.so")
    loaded_params = bytearray(open(model_path + "deploy_param.params", "rb").read())
    model = graph_runtime.create(loaded_json, loaded_lib, ctx)
    model.load_params(loaded_params)
    return model

def load_model_config(model, cpu_cores):
    """
    Loads configuration file for the ML model and sets up functions & parameters to use for selected model in inference
    Supported models are: yolov3, tiny_yolov3

    Args:
        model (string): command line parameter selecting which model to configure for
        cpu_cores (int):  command line parameter setting how many threads the TVM library should use in inference 

    Returns:
        postprocess (function): postprocessing function for supported models
        score_threshold (float): Minimum accuracy score for an object
        mask (int-matrix): Matrix used for ML masking
        anchors (int-matrix): Marix containing predefined bounding boxes known as anchor boxes
        model_path (string): Location of model data
        max_outputs (int): Max objects to be detected in a single frame. Currently set a "100" 
        iou_threshold (float): Threshold for the Intersection over Union (IoU) algorthim 
        names (string): Location of file which contains list of possible objects that can be detected by the model
        strides (int array): Configures number and size of stride layers for downsampling the image 
        num_classes (int): Total number in objects the model can detect
        normalize_and_reshape (function): Configures frame to ensure it's compatable with model
    """
    yolov3_config = json.load(open("config/yolov3.json"))
    tiny_yolov3_config = json.load(open("config/tiny_yolov3.json"))

    os.environ["TVM_NUM_THREADS"] = cpu_cores

    if model == "tiny_yolov3":                                                                         
        model_path = tiny_yolov3_config["model_path"]                                                                              
        class_name_path = tiny_yolov3_config["class_name_path"]                                                                       
        strides = tiny_yolov3_config["strides"]                                                                                                   
        anchors = tiny_yolov3_config["anchors"]                                                        
        mask = tiny_yolov3_config["mask"]                                                                                 
        score_threshold = float(tiny_yolov3_config["score_threshold"])                                                                       
        postprocess = postprocess_tiny                                                                                          
    elif model == "yolov3":                                                                         
        model_path = yolov3_config["model_path"]                                              
        class_name_path = yolov3_config["class_name_path"]                                                     
        strides =  yolov3_config["strides"]                                                                                             
        anchors =  yolov3_config["anchors"]                                       
        mask = yolov3_config["mask"]                                                                                             
        score_threshold = float(yolov3_config["score_threshold"])                                                                      
        postprocess = postprocess_yolo                                                          
    else:                                                                                           
        print("Model Not supported...Selecting the yolov3 model")               
        model_path = yolov3_config["model_path"]                                                                     
        class_name_path = yolov3_config["class_name_path"]                                              
        strides =  yolov3_config["strides"]                                                                                   
        anchors =  yolov3_config["anchors"]                                 
        mask = yolov3_config["mask"]                                                                                      
        score_threshold = float(yolov3_config["score_threshold"])                                                             
        postprocess = postprocess_yolo


    max_outputs = 100                                                                               
    iou_threshold = 0.4 
    names = decode_class_names(class_name_path)                                                     
    num_classes = len(names)                                          
    anchors = np.array(list(map(lambda x: list(map(int, str.split(x, ','))), anchors.split())))     
    mask = np.array(list(map(lambda x: list(map(int, str.split(x, ','))), mask.split())))           
    strides = list(map(int, strides.split(',')))


    return postprocess, score_threshold, mask, anchors, model_path, max_outputs, iou_threshold, names , strides, num_classes
