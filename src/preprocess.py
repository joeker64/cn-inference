"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

"""
preprocess.py:
    Computation needed to prepare frame or data before inference
    Supported models are: yolov3, yolov3_tiny 
"""
import numpy as np
import cv2

def decode_class_names(classes_path):
    """
    Reads the list of objects trained in the ML model into an array

    Args:
        classes_path (string): Path of the classes file
    
    Returns:
        classes (string array): Array of what objects the ml model has been trained to detect
    """
    with open(classes_path, 'r') as f:
        lines = f.readlines()
    classes = []
    for line in lines:
        line = line.strip()
        if line:
            classes.append(line)
    return classes

def normalize_and_reshape(img):
    """
    Reshapes image to match with ML training config

    Args:
        img (cv.Mat): Frame from video feed
    Returns:
        img (cv.Mat): Altered image to match dimensions for ML model
    """
    img = img.astype(np.float32)                       
    img = np.reshape(img, (1,416,416,3))
    return img